package com.hillel.hw1.repository;

import com.hillel.hw1.entity.BasicEntity;

import java.util.List;
import java.util.Optional;

public interface CRUDRepository<T extends BasicEntity> {

    void add(T entity);

    Optional<T> findById(int id);

    void update(T entity);

    void delete(T entity);

    List<T> getAll();

}
