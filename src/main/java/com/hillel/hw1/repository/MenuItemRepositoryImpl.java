package com.hillel.hw1.repository;

import com.hillel.hw1.entity.MenuItem;

import java.util.List;
import java.util.stream.Collectors;

public class MenuItemRepositoryImpl extends CRUDRepositoryImpl<MenuItem> implements MenuItemRepository {

    @Override
    public List<MenuItem> getItemsByMenuID(final int menuID) {
        return repository.stream().filter(item -> item.getMenuID() == menuID).collect(Collectors.toList());
    }
}
