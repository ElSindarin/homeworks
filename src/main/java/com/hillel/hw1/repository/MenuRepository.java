package com.hillel.hw1.repository;

import com.hillel.hw1.entity.Menu;

public interface MenuRepository extends CRUDRepository<Menu> {
}
