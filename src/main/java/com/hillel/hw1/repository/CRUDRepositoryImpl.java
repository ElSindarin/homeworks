package com.hillel.hw1.repository;

import com.hillel.hw1.entity.BasicEntity;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

public class CRUDRepositoryImpl<T extends BasicEntity> implements CRUDRepository<T> {
    protected final List<T> repository = new CopyOnWriteArrayList<>();
    private int idGenerator = 1;

    @Override
    public void add(final T entity) {
        entity.setId(idGenerator++);
        repository.add(entity);
    }

    @Override
    public Optional<T> findById(final int id) {
        return repository.stream().filter(entity->entity.getId() == id).findAny();
    }

    @Override
    public void update(final T entity) {
        repository.set(repository.indexOf(findById(entity.getId()).get()), entity);
    }

    @Override
    public void delete(final T entity) {
        repository.remove(entity);
    }

    @Override
    public List<T> getAll() {
        return repository;
    }

    public List<T> getRepository() {
        return repository;
    }

}
