package com.hillel.hw1.repository;

import com.hillel.hw1.entity.MenuItem;

import java.util.List;

public interface MenuItemRepository extends CRUDRepository<MenuItem> {
    List<MenuItem> getItemsByMenuID (final int sectionID);
}