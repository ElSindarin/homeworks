package com.hillel.hw1.exception;

import java.util.UUID;

public class EntityNotFoundException extends RuntimeException{
    private static final UUID serialVersionUID = UUID.randomUUID();
    public EntityNotFoundException(final String message) {
        super(message);
    }
}
