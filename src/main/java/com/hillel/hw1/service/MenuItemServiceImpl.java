package com.hillel.hw1.service;

import com.hillel.hw1.entity.Menu;
import com.hillel.hw1.entity.MenuItem;
import com.hillel.hw1.repository.MenuItemRepository;
import com.hillel.hw1.repository.MenuRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MenuItemServiceImpl extends CommonServiceImpl<MenuItem, MenuItemRepository> implements MenuItemService {

    private MenuRepository menuRepository;

    public MenuItemServiceImpl(MenuItemRepository repository, MenuRepository menuRepository) {
        super(repository);
        this.menuRepository = menuRepository;
    }


    @Override
    public void add(final MenuItem menuItem) {
        final Optional<Menu> menu = menuRepository.findById(menuItem.getMenuID());
        menu.ifPresentOrElse((Menu item) -> {
            final boolean isDuplicated = repository.getItemsByMenuID(menuItem.getMenuID()).stream().anyMatch(mItem -> mItem.getTitle().equals(menuItem.getTitle()));
            if (isDuplicated) {
                throw new IllegalArgumentException("This item is already in the menu");
            }
            repository.add(menuItem);
        }, () -> {
            throw new IllegalArgumentException("This menu does not exist");
        });
    }

    @Override
    public List<MenuItem> getItemsByMenuID(final int menuID) {
        return repository.getItemsByMenuID(menuID);
    }
}
