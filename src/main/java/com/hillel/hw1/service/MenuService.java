package com.hillel.hw1.service;

import com.hillel.hw1.entity.Menu;

import java.math.BigDecimal;

public interface MenuService extends CommonService<Menu> {
    boolean updatePrice(int menuItemID, BigDecimal newPrice);

    void delete(Menu menu);
}
