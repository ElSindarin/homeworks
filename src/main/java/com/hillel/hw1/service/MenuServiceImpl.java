package com.hillel.hw1.service;

import com.hillel.hw1.entity.Menu;
import com.hillel.hw1.entity.MenuItem;
import com.hillel.hw1.repository.MenuRepository;

import java.math.BigDecimal;
import java.util.Optional;

public class MenuServiceImpl extends CommonServiceImpl<Menu, MenuRepository> implements MenuService {

    private final transient MenuItemService menuItemService;
    private static final String ITEM_NOT_EXISTS = "MenuItem not exists";

    public MenuServiceImpl(final MenuRepository menuRep, final MenuItemService itemService) {
        super(menuRep);
        this.menuItemService = itemService;
    }

    @Override
    public boolean updatePrice(final int menuItemID, final BigDecimal newPrice) {
        if (newPrice.compareTo(BigDecimal.ZERO) <= -1) {
            throw new IllegalArgumentException("Cannot update by a non-positive price");
        }
        menuItemService.findById(menuItemID).ifPresentOrElse((MenuItem item) -> {
            item.setPrice(newPrice);
            menuItemService.update(item);
        }, () -> {
            throw new IllegalArgumentException(ITEM_NOT_EXISTS);
        });

        return true;
    }

    @Override
    public void delete(final Menu menu) {
        final Optional<Menu> optionalMenu = repository.findById(menu.getId());
        optionalMenu.ifPresentOrElse((Menu mItem) -> {
            menuItemService.getItemsByMenuID(mItem.getId()).forEach(menuItemService::delete);
            repository.delete(optionalMenu.get());
        }, () -> {
            throw new IllegalArgumentException("There is no such menu to delete");
        });
    }
}