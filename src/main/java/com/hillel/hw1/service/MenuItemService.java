package com.hillel.hw1.service;

import com.hillel.hw1.entity.MenuItem;

import java.util.List;

public interface MenuItemService extends CommonService<MenuItem> {
    void add(MenuItem menuItem);

    List<MenuItem> getItemsByMenuID(int menuID);
}