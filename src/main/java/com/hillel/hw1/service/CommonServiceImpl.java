package com.hillel.hw1.service;

import com.hillel.hw1.entity.BasicEntity;
import com.hillel.hw1.exception.EntityNotFoundException;
import com.hillel.hw1.repository.CRUDRepository;

import java.util.List;
import java.util.Optional;

public class CommonServiceImpl<E extends BasicEntity, R extends CRUDRepository<E>> implements CommonService<E> {
    public static final String ENTITY_OF_CLASS = "Entity of class";
    public static final String IS_NOT_IN_REPO = "is not in the repository named";
    protected final transient R repository;

    public CommonServiceImpl(final R repository) {
        this.repository = repository;
    }

    @Override
    public void add(final E entity) {
        findById(entity.getId()).ifPresentOrElse((E thisEntity) -> {
            throw new EntityNotFoundException(ENTITY_OF_CLASS + " " + thisEntity.getClass().getSimpleName() + " is already in its repository named " + repository.getClass().getSimpleName());
        }, () -> repository.add(entity));
    }

    @Override
    public void update(final E entity) {
        findById(entity.getId()).ifPresentOrElse(item -> repository.update(entity), () -> {
            throw new EntityNotFoundException(ENTITY_OF_CLASS + " " + entity.getClass().getSimpleName() + " " + IS_NOT_IN_REPO + " " + repository.getClass().getSimpleName());
        });
    }

    @Override
    public void delete(final E entity) {
        findById(entity.getId()).ifPresentOrElse(repository::delete, () -> {
            throw new EntityNotFoundException(ENTITY_OF_CLASS + " " + entity.getClass().getSimpleName() + " " + IS_NOT_IN_REPO + " " + repository.getClass().getSimpleName());
        });
    }

    @Override
    public List<E> getAll() {
        return repository.getAll();
    }

    @Override
    public Optional<E> findById(final int id) {
        return repository.findById(id);
    }
}