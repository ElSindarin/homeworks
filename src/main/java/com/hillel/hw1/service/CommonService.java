package com.hillel.hw1.service;

import com.hillel.hw1.entity.BasicEntity;

import java.util.List;
import java.util.Optional;

public interface CommonService<E extends BasicEntity> {

    void add(E entity);

    void update(E entity);

    void delete(E entity);

    List<E> getAll();

    Optional<E> findById(int id);
}
