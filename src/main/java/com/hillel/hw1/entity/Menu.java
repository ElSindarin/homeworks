package com.hillel.hw1.entity;

import java.util.List;

public class Menu extends BasicEntity{
    private String title;
    private String description;
    private List<MenuItem> menuItems;

    public Menu(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", menuItems=" + menuItems +
                '}';
    }
}
