package com.hillel.hw1.entity;

import java.math.BigDecimal;

public class MenuItem extends BasicEntity {
    private String title;
    private String description;
    private BigDecimal price;
    private int menuID;

    public MenuItem(String title, String description, BigDecimal price, int menuID) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.menuID = menuID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getMenuID() {
        return menuID;
    }

    public void setMenuID(int menuID) {
        this.menuID = menuID;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", menuID=" + menuID +
                '}';
    }
}
