import com.hillel.hw1.entity.Menu;
import com.hillel.hw1.entity.MenuItem;
import com.hillel.hw1.repository.MenuItemRepository;
import com.hillel.hw1.repository.MenuItemRepositoryImpl;
import com.hillel.hw1.repository.MenuRepository;
import com.hillel.hw1.repository.MenuRepositoryImpl;
import com.hillel.hw1.service.MenuItemService;
import com.hillel.hw1.service.MenuItemServiceImpl;
import com.hillel.hw1.service.MenuService;
import com.hillel.hw1.service.MenuServiceImpl;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {

        final MenuRepository menuRepository = new MenuRepositoryImpl();
        final MenuItemRepository menuItemRepository = new MenuItemRepositoryImpl();
        final MenuItemService menuItemService = new MenuItemServiceImpl(menuItemRepository, menuRepository);
        final MenuService menuService = new MenuServiceImpl(menuRepository, menuItemService);

        Menu menu1 = new Menu("Menu 1", "Description Menu 1");
        Menu menu2 = new Menu("Menu 2", "Description Menu 2");
        Menu menu3 = new Menu("Menu 3", "Description Menu 3");
        Menu menu4 = new Menu("Menu 4", "Description Menu 4");

        menuService.add(menu1);
        menuService.add(menu2);
        menuService.add(menu3);
        menuService.add(menu4);

        MenuItem menuItem1 = new MenuItem("Borsh classic", "Red Borsh", new BigDecimal(100), menu1.getId());
        MenuItem menuItem2 = new MenuItem("Borsh green", "Green Borsh", new BigDecimal(95), menu1.getId());
        MenuItem menuItem3 = new MenuItem("Holodec", "Holodec from fish", new BigDecimal(80), menu2.getId());
        MenuItem menuItem4 = new MenuItem("Fanta", "Beverages - stomach drinks", new BigDecimal(20), menu3.getId());
        MenuItem menuItem5 = new MenuItem("Lays", "Sores, heartburn and mega crunch.", new BigDecimal(10), menu4.getId());

        menuItemService.add(menuItem1);
        menuItemService.add(menuItem2);
        menuItemService.add(menuItem3);
        menuItemService.add(menuItem4);
        menuItemService.add(menuItem5);

        System.out.println(menuService.getAll());
        System.out.println(menuItemService.getItemsByMenuID(1).toString());
    }
}
